build:
	
	@echo "building nginx image"
	@sleep 2
	docker build -t mynginx nginx/
run:
	docker compose up -d --build

remove:
	docker compose down -v
	docker image rm mynginx

env:
	@read -p "Password ?: " PASS; echo "ROOT_PASSWORD="$$PASS > .env	
